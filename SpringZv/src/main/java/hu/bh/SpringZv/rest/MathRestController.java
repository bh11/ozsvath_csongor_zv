package hu.bh.SpringZv.rest;

import hu.bh.SpringZv.dto.MathRequest;
import hu.bh.SpringZv.dto.MathResponse;
import hu.bh.SpringZv.service.MathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/calculator")
public class MathRestController {

    @Autowired
    private MathService service;



    @RequestMapping(value = "/math", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity filter(@RequestBody MathRequest request) {
        int sum = service.calculate(request.getX(),request.getOperator() ,request.getY());
        MathResponse mathResponse = new MathResponse();
        mathResponse.setSum(sum);
        return ResponseEntity.ok(mathResponse);
    }
}