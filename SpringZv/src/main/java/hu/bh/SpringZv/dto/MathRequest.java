package hu.bh.SpringZv.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class MathRequest implements Serializable {
    @JsonProperty("_x")
    private int x;
    @JsonProperty("_operator")
    private String operator;
    @JsonProperty("_y")
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MathRequest that = (MathRequest) o;
        return x == that.x &&
                y == that.y &&
                Objects.equals(operator, that.operator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, operator, y);
    }


    @Override
    public String toString() {
        return "MathRequest{" +
                "x=" + x +
                ", operator='" + operator + '\'' +
                ", y=" + y +
                '}';
    }
}
