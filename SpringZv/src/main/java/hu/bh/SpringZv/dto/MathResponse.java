package hu.bh.SpringZv.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class MathResponse implements Serializable {
    @JsonProperty("_sum")
    private int sum;

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}
