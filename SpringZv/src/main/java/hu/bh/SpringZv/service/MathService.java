package hu.bh.SpringZv.service;

import hu.bh.SpringZv.exception.BadOperatorexception;
import org.springframework.stereotype.Service;

@Service
public class MathService {

    private static final String MINUS = "-";

    public int calculate(int x, String operator, int y) {
        int sum  = 0;

        if(operator.equals("+")){
            sum = plus(x,y);
        }

        if(MINUS.equals(operator)){
            sum = minus(x,y);

        } if(operator.equals("*")){
            sum = multiply(x,y);

        } if(operator.equals("/")){
            sum = divide(x,y);
        }

        if(!operator.equals("+") && !operator.equals("-") && !operator.equals("/") && !operator.equals("*")){
            badOperator();
        }

        if ( operator.equals("/") && (y==0)){
            devideWithZero();
        }


        return sum;
    }

    public int plus(int x,  int y){
        return x+y;
    }
    public int minus(int x,  int y){
        return x-y;
    }
    public int multiply(int x,  int y){
        return x*y;
    }
    public int divide(int x,  int y){
        return x/y;
    }
    public void badOperator(){
        throw new BadOperatorexception("Not a valid math expression!");
    }
    public void devideWithZero(){
        throw new BadOperatorexception("You can't devide with 0!");
    }
}
