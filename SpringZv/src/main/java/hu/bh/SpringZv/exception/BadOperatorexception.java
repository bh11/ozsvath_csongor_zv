package hu.bh.SpringZv.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;



    @ResponseStatus(HttpStatus.NOT_FOUND)
    public class BadOperatorexception extends RuntimeException {

        public BadOperatorexception(String message) {
            super(message);
        }
    }

