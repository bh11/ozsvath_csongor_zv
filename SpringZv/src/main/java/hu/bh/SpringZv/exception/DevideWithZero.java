package hu.bh.SpringZv.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class DevideWithZero extends RuntimeException {

    public DevideWithZero(String message) {
        super(message);
    }
}

