package hu.bh.SpringZv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringZvApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringZvApplication.class, args);
	}

}
