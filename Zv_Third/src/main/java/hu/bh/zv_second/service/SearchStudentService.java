package hu.bh.zv_second.service;

import hu.bh.zv_second.repository.dao.ABCDao;
import hu.bh.zv_second.repository.dao.StudentDao;
import hu.bh.zv_second.repository.entity.ABC;
import hu.bh.zv_second.repository.entity.Student;
import lombok.Setter;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
@Setter
public class SearchStudentService {

    @Inject
    private StudentDao studentDao;

    @Inject
    private ABCDao abcDao;

    public List<Student> getStudentsByLetter(String letter){
        List<Student> students = getAllStudent();
        List<Student> filteredStudents = new ArrayList<>();
        for (Student student : students) {
            if (student.getFirstName().startsWith(letter)){
                filteredStudents.add(student);
            }
        }
        return filteredStudents;
    }

    public List<Student> getAllStudent(){
        return studentDao.findAll();
    }

    public List<ABC> getAllLetters(){
        return abcDao.findAll();
    }


}
