package hu.bh.zv_second.repository.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Grade {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Double grade;

    @Enumerated(value = EnumType.STRING)
    private ExamType examType;


    @ManyToOne
    private Student student;
}
