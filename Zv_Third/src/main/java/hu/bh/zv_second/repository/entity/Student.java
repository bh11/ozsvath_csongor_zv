package hu.bh.zv_second.repository.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Student {

    @Id
    private String email;
    private String firstName;
    private String lastName;


    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "grade_id")
    private List<Grade> grades;
}
