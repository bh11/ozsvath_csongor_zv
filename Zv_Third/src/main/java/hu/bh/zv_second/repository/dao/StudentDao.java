package hu.bh.zv_second.repository.dao;

import hu.bh.zv_second.repository.entity.Student;
import lombok.Data;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;


@Stateless
@LocalBean
@Data
public class StudentDao implements CRUDRepository<Student, Integer> {

    @PersistenceContext
    private EntityManager em;


    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Student entity) {
        em.remove(entity);
    }

    @Override
    public void deleteAll() {

    }

    @Override
    public void deleteAll(Iterable<? extends Student> entities) {

    }

    @Override
    public void deleteById(Integer id) {

    }


    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public List<Student> findAll() {
        return em.createQuery("SELECT a FROM Student a")
                .getResultList();    }

    @Override
    public Iterable<Student> findAllById(Iterable<Integer> integers) {
        return null;
    }

    @Override
    public Optional<Student> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public <S extends Student> S save(S entity) {
        em.persist(entity);
        return null;
    }


    @Override
    public <S extends Student> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }
}