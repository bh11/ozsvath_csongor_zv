package hu.bh.zv_second.repository.entity;

public enum ExamType {
    FIRSTEXAM,
    SECONDEXAM,
    FINALEXAM
}
