/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.zv_second.servlet;


import hu.bh.zv_second.repository.entity.ABC;
import hu.bh.zv_second.repository.entity.Student;
import hu.bh.zv_second.service.SearchStudentService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Csongi
 */
@WebServlet(name = "Abc1", urlPatterns = {"/Abc1"})
public class Abc1Servlet extends HttpServlet {




    @Inject
    SearchStudentService service;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Student student = (Student)service.getStudentsByLetter(service.getAllLetters().get(0).getLetter()).get(0);
        String a= request.getParameter("param1");


        request.setAttribute("student" ,student);
        request.setAttribute("a" , a);

        request.getRequestDispatcher("result.jsp").forward(request, response);
    }




}
