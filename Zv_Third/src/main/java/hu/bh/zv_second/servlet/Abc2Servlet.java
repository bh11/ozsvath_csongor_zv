/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.zv_second.servlet;


import hu.bh.zv_second.repository.entity.Student;
import hu.bh.zv_second.service.SearchStudentService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Csongi
 */
@WebServlet(name = "Abc2", urlPatterns = {"/Abc2"})
public class Abc2Servlet extends HttpServlet {




    @Inject
    SearchStudentService service;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Student student = (Student)service.getStudentsByLetter(service.getAllLetters().get(1).getLetter()).get(0);

        request.setAttribute("student" ,student);
        request.getRequestDispatcher("result.jsp").forward(request, response);
    }




}
